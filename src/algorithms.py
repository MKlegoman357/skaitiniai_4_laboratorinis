import math

import numpy as np

import matrices


def range_uniform_steps(x_from, x_to, step=None, steps=None):
    if steps is None:
        steps = round((x_to - x_from) / step)
    return np.linspace(x_from, x_to, steps)


def heming_value(x_values, coefficients):
    y = 0
    for i, coefficient in enumerate(coefficients):
        y += coefficient * x_values[i]
    return y


def adjust_to_chebyshev_argument(real_x, x_from, x_to):
    return (2 * real_x - x_to - x_from) / (x_to - x_from)


def adjust_from_chebyshev_argument(chebyshev_x, x_from, x_to):
    return ((x_to - x_from) * chebyshev_x + x_to + x_from) / 2


def range_by_chebyshev(x_from, x_to, steps):
    return adjust_from_chebyshev_argument(
        np.array(
            [math.cos(math.pi * (2 * i + 1) / (2 * steps)) for i in range(steps)]
        ), x_from, x_to)


def chebyshev_values(x, n):
    values = [1, x]
    for i in range(2, n):
        values.append(2 * x * values[i - 1] - values[i - 2])
    return values


def chebyshev_matrix(x_values):
    return np.array([chebyshev_values(x, len(x_values)) for x in x_values])


def construct_chebyshev_function(x_values, y_values):
    x_from, x_to = min(x_values), max(x_values)
    chebyshev_x_values = adjust_to_chebyshev_argument(x_values, x_from, x_to)
    y_values = np.array([y_values]).T

    matrix = chebyshev_matrix(chebyshev_x_values)
    augmented_gaussian_matrix = np.concatenate((matrix, y_values), axis=1).astype(float)
    augmented_gaussian_matrix = matrices.gauss_jordan_elimination(augmented_gaussian_matrix)
    a = augmented_gaussian_matrix[:, [-1]].T[0]

    return lambda x: heming_value(chebyshev_values(adjust_to_chebyshev_argument(x, x_from, x_to), len(a)), a)


def calculate_akima_values(x, xi, xim1, xip1, yi, yim1, yip1):
    a = (2 * x - xi - xip1) / ((xim1 - xi) * (xim1 - xip1))
    b = (2 * x - xim1 - xip1) / ((xi - xim1) * (xi - xip1))
    c = (2 * x - xim1 - xi) / ((xip1 - xim1) * (xip1 - xi))
    return a * yim1 + b * yi + c * yip1


def akima_values(x_values, y_values):
    n = len(x_values)
    dy_values = []
    for i in range(n):
        if i == 0:
            xi, xim1, xip1 = x_values[1], x_values[0], x_values[2]
            yi, yim1, yip1 = y_values[1], y_values[0], y_values[2]
            dy_values.append(calculate_akima_values(xim1, xi, xim1, xip1, yi, yim1, yip1))
        elif i == n - 1:
            xi, xim1, xip1 = x_values[n - 2], x_values[n - 3], x_values[n - 1]
            yi, yim1, yip1 = y_values[n - 2], y_values[n - 3], y_values[n - 1]
            dy_values.append(calculate_akima_values(xip1, xi, xim1, xip1, yi, yim1, yip1))
        else:
            xi, xim1, xip1 = x_values[i], x_values[i - 1], x_values[i + 1]
            yi, yim1, yip1 = y_values[i], y_values[i - 1], y_values[i + 1]
            dy_values.append(calculate_akima_values(xi, xi, xim1, xip1, yi, yim1, yip1))
    return dy_values


def lagrange(x, j, x_values):
    n = len(x_values)
    l = 1
    for i in range(n):
        if i != j:
            l *= (x - x_values[i]) / (x_values[j] - x_values[i])
    return l


def d_lagrange(x, j, x_values):
    n = len(x_values)
    dl = 0
    for k in range(n):
        if k != j:
            lds = 1
            for i in range(n):
                if i != j and i != k:
                    lds *= x - x_values[i]
            dl += lds
    ldv = 1
    for i in range(n):
        if i != j:
            ldv *= x_values[j] - x_values[i]
    return dl / ldv


def hermite_function(x, j, x_values):
    lagrange_j = lagrange(x, j, x_values)
    d_lagrange_j = d_lagrange(x_values[j], j, x_values)
    x_xj_delta = x - x_values[j]
    lagrange2_j = lagrange_j ** 2

    u = (1 - 2 * d_lagrange_j * x_xj_delta) * lagrange2_j
    v = x_xj_delta * lagrange2_j

    return u, v


def get_hermite_spline(x_values, y_values, steps):
    dy_values = akima_values(x_values, y_values)

    hermite_spline_x = []
    hermite_spline_y = []
    for i in range(len(x_values) - 1):
        spline_x = np.linspace(x_values[i], x_values[i + 1], steps)
        spline_y = 0
        for j in range(2):
            u, v = hermite_function(spline_x, j, x_values[i:i + 2])
            spline_y += u * y_values[i + j] + v * dy_values[i + j]

        hermite_spline_x += list(spline_x)
        hermite_spline_y += list(spline_y)

    return hermite_spline_x, hermite_spline_y


def simple_approximation_base(number_of_functions, x_values):
    return np.array([[x ** i for i in range(number_of_functions)] for x in x_values])


def get_simple_approximation_weights(x_values, y_values, number_of_functions):
    g = simple_approximation_base(number_of_functions, x_values)

    y_values = np.array([y_values]).T
    augmented_gaussian_matrix = np.concatenate((np.matmul(g.T, g), np.matmul(g.T, y_values)), axis=1).astype(float)
    augmented_gaussian_matrix = matrices.gauss_jordan_elimination(augmented_gaussian_matrix)
    c = augmented_gaussian_matrix[:, [-1]]

    return c


def construct_simple_approximation_function(c):
    return lambda x: np.matmul(np.array([x ** i for i in range(len(c))]), c)


def euler_differential_system(functions, f0, x_values, additional_parameters=None, delta_x=0, stop_below=()):
    if additional_parameters is None:
        additional_parameters = [[] for _ in functions]

    values = [[] for _ in functions]
    current_values = f0[:]

    for i, x in enumerate(x_values):
        for j, f in enumerate(functions):
            y = current_values[j]
            current_values[j] = y + delta_x * f(x, y, *reversed(current_values[:j]), *additional_parameters[j])
            values[j].append(current_values[j])

        should_stop = False
        for j, stop in enumerate(stop_below):
            if stop is not None and current_values[j] < stop:
                should_stop = True
                break

        if should_stop:
            break

        if i < len(x_values) - 1:
            delta_x = x_values[i + 1] - x_values[i]

    return values


def runge_kutta_4_differential_system(functions, f0, x_values, additional_parameters=None, delta_x=0, stop_below=()):
    if additional_parameters is None:
        additional_parameters = [[] for _ in functions]

    values = [[] for _ in functions]
    current_values = f0[:]

    for i, x in enumerate(x_values):
        for j, f in enumerate(functions):
            f_params = [*reversed(current_values[:j]), *additional_parameters[j]]
            y = current_values[j]

            f1 = f(x, y, *f_params)
            y1 = y + delta_x / 2 * f1

            f2 = f(x + delta_x / 2, y1, *f_params)
            y2 = y + delta_x / 2 * f2

            f3 = f(x + delta_x / 2, y2, *f_params)
            y3 = y + delta_x * f3

            f4 = f(x + delta_x, y3, *f_params)
            y4 = y + delta_x / 6 * (f1 + 2 * f2 + 2 * f3 + f4)

            current_values[j] = y4
            values[j].append(current_values[j])

        should_stop = False
        for j, stop in enumerate(stop_below):
            if stop is not None and current_values[j] < stop:
                should_stop = True
                break

        if should_stop:
            break

        if i < len(x_values) - 1:
            delta_x = x_values[i + 1] - x_values[i]

    return values
