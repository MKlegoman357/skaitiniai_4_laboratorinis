import matplotlib.pyplot as plt

import algorithms

m1 = 120
m2 = 10
tg = 25
k1 = 0.25
k2 = 10
g = 9.8

h0 = 2500
v0 = 0
t0 = 0

m = m1 + m2


def dv(t, v, k):
    """
    dv(t,v)=(mg-k*v^2)/m
    """
    return (m * g - k * (v ** 2)) / m


def dh(t, h, v):
    """
    dh1(t,h,v)=-v
    """
    return -v


def solution(method, differential_system_func, t1_step=0.1, t2_step=0.1, t_stop=130, check_step=False):
    title = f"{method} (t zingsniai {t1_step} ir {t2_step})"
    print(f"{title}:")

    plt.figure()

    t1_values = algorithms.range_uniform_steps(t0, tg, step=t1_step)
    v1_values, h1_values = differential_system_func(
        functions=[dv, dh], f0=[v0, h0], x_values=t1_values, additional_parameters=[[k1], []]
    )

    plt.plot(t1_values, v1_values, label="v - laisvas kritimas")
    plt.plot(t1_values, h1_values, label="h - laisvas kritimas")

    print("Last values on free-fall:")
    print(f"t1_1 = {t1_values[-1]} s")
    print(f"v1_1 = {v1_values[-1]} m/s")
    print(f"h1_1 = {h1_values[-1]} m")

    if check_step:
        t1_values2 = algorithms.range_uniform_steps(t0, tg, step=t1_step / 2)
        v1_values2, h1_values2 = differential_system_func(
            functions=[dv, dh], f0=[v0, h0], x_values=t1_values2, additional_parameters=[[k1], []]
        )

        plt.plot(t1_values2, v1_values2, label="v - laisvas kritimas (2 kartus mazesnis zingsnis)")
        plt.plot(t1_values2, h1_values2, label="h - laisvas kritimas (2 kartus mazesnis zingsnis)")

        print("Last values on free-fall (2 times smaller step):")
        print(f"t1_2 = {t1_values2[-1]} s")
        print(f"v1_2 = {v1_values2[-1]} m/s")
        print(f"h1_2 = {h1_values2[-1]} m")

        print("Difference:")
        print(f"|t1_1 - t1_2| = {abs(t1_values[-1] - t1_values2[-1])} s")
        print(f"|v1_1 - v1_2| = {abs(v1_values[-1] - v1_values2[-1])} m/s")
        print(f"|h1_1 - h1_2| = {abs(h1_values[-1] - h1_values2[-1])} m")

    print()

    t2_values = algorithms.range_uniform_steps(tg, t_stop, step=t2_step)
    v2_values, h2_values = differential_system_func(
        functions=[dv, dh], f0=[v1_values[-1], h1_values[-1]], x_values=t2_values, additional_parameters=[[k2], []],
        stop_below=[None, 0]
    )

    t2_values = t2_values[:len(h2_values)]

    plt.plot(t2_values, v2_values, label="v - parasiutu")
    plt.plot(t2_values, h2_values, label="h - parasiutu")

    print("Last values after going below h=0 (hitting the ground):")
    print(f"t2_1 = {t2_values[-1]} s")
    print(f"v2_1 = {v2_values[-1]} m/s")
    print(f"h2_1 = {h2_values[-1]} m")

    if check_step:
        t2_values2 = algorithms.range_uniform_steps(tg, t_stop, step=t2_step / 2)
        v2_values2, h2_values2 = differential_system_func(
            functions=[dv, dh], f0=[v1_values2[-1], h1_values2[-1]], x_values=t2_values2,
            additional_parameters=[[k2], []],
            stop_below=[None, 0]
        )

        t2_values2 = t2_values2[:len(h2_values2)]

        plt.plot(t2_values2, v2_values2, label="v - parasiutu (2 kartus mazesnis zingsnis)")
        plt.plot(t2_values2, h2_values2, label="h - parasiutu (2 kartus mazesnis zingsnis)")

        print("Last values after going below h=0 (hitting the ground, 2 times smaller step):")
        print(f"t2_2 = {t2_values2[-1]} s")
        print(f"v2_2 = {v2_values2[-1]} m/s")
        print(f"h2_2 = {h2_values2[-1]} m")

        print("Difference:")
        print(f"|t2_1 - t2_2| = {abs(t2_values[-1] - t2_values2[-1])} s")
        print(f"|v2_1 - v2_2| = {abs(v2_values[-1] - v2_values2[-1])} m/s")
        print(f"|h2_1 - h2_2| = {abs(h2_values[-1] - h2_values2[-1])} m")

    print()
    plt.legend()
    plt.title(title)


def euler_solution(t1_step=0.1, t2_step=0.1, t_stop=130, check_step=False):
    return solution("Eulerio metodas", algorithms.euler_differential_system, t1_step, t2_step, t_stop, check_step)


def runge_kutta_solution(t1_step=0.1, t2_step=0.1, t_stop=130, check_step=False):
    return solution("Runge-Kutta 4 eiles metodas", algorithms.runge_kutta_4_differential_system, t1_step, t2_step, t_stop, check_step)


if __name__ == "__main__":
    euler_solution(0.1, 0.1, check_step=True)
    runge_kutta_solution(0.1, 0.1, check_step=True)

    plt.show()
